# delivery-management

This is a simple project where you can reserve a delivery slot and get a list of possible delivery types...

### Getting Started
First run `docker-compose up` to start the project.

### Usage
You can go to `localhost:8080/api/v1/reservations/delivery-types` to get the list of possible delivery types.
curt command: `curl localhost:8080/api/v1/reservations/delivery-type` in the terminal.

Make a delivery reservation by running the following command:
```agsl
curl -X 'POST' \
  'http://localhost:8080/api/v1/reservations/reserve' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d '{
    "userId": 1,
    "slotStart": "2024-02-13T21:47:45.947Z",
    "slotEnd": "2024-02-13T21:47:45.947Z",
    "address": "string",
    "note": "string",
    "deliveryType": "DELIVERY"
  }'
```

### Messaging
When you reserve a slot, a new reservation gets created and a message is sent to the `created-reservation` topic.
To see the messages coming in, you can run the following that enables you to listen to Kafka messages:
```
docker exec -ti kafka /opt/kafka/bin/kafka-console-consumer.sh \
  --bootstrap-server localhost:9092 \
  --topic created-reservation \
  --from-beginning
```


### TODO
Unit tests

Deploy to k8s

Native image

Secure the api
