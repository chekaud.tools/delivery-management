package com.dm.delivery.reservation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CreatedReservationsMessagesProducer {

    private final static String CREATED_RESERVATION_TOPIC_NAME = "created-reservation";
    private final ObjectMapper objectMapper;
    private final KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessage(Object obj) {
        try {
            kafkaTemplate.send(CREATED_RESERVATION_TOPIC_NAME, this.objectMapper.writeValueAsString(obj));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
