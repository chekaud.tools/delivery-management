package com.dm.delivery.service;

import com.dm.delivery.domain.SlotReservation;
import com.dm.delivery.domain.enums.DeliveryType;
import com.dm.delivery.domain.enums.ReservationStatus;
import com.dm.delivery.dto.ReserveDto;
import com.dm.delivery.dto.UpdateReservationDto;
import com.dm.delivery.dto.mapper.SlotReservationMapper;
import com.dm.delivery.repository.SlotReservationRepository;
import com.dm.delivery.reservation.CreatedReservationsMessagesProducer;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.dm.delivery.utils.CacheConstants.CACHE_KEY;
import static com.dm.delivery.utils.CacheConstants.CACHE_RESERVATION;

@Service
@AllArgsConstructor
public class SlotReservationService {

    private final SlotReservationRepository slotReservationRepository;
    private final SlotReservationMapper slotReservationMapper;
    private final CreatedReservationsMessagesProducer createdReservationsMessagesProducer;

    public Flux<SlotReservation> findAllReservationsByUserId(Long userId) {
        return slotReservationRepository.findAllByUserId(userId);
    }

    public Flux<SlotReservation> findAllReservationsByUserIdAndStatus(Long userId, ReservationStatus status) {
        return slotReservationRepository.findAllByUserIdAndStatus(userId, status);
    }

    public Mono<SlotReservation> getReservation(String id) {
        return slotReservationRepository.findById(id);
    }

    public Mono<List<String>> getDeliveryTypes() {
        List<String> stringValues = Arrays.stream(DeliveryType.values())
                .map(Enum::toString)
                .toList();
        return Mono.just(stringValues);
    }

    public Mono<SlotReservation> cancelReservation(String id) {
        return slotReservationRepository.findById(id)
                .flatMap(reservation -> {
                    reservation.setStatus(ReservationStatus.CANCELED);
                    return slotReservationRepository.save(reservation);
                });
    }

    public Mono<SlotReservation> updateReservationStatus(String reservationId, UpdateReservationDto request) {
        return this.getReservation(reservationId).flatMap(reservation -> {
            reservation.setStatus(ReservationStatus.valueOf(request.getStatus()));
            return slotReservationRepository.save(reservation);
        });
    }

    public Mono<String> getStatus(UUID id) {
        return slotReservationRepository.findById(id.toString()).map(SlotReservation::getStatus).map(ReservationStatus::getStatus);
    }

    @CacheEvict(value = CACHE_RESERVATION, key = "{" + CACHE_KEY + ", #request.userId}")
    public Mono<SlotReservation> reserveSlot(ReserveDto request) {
        if (checkSlotsValidity(request)) {
            SlotReservation slotReservation = slotReservationMapper.fromMakeReservationRequest(request);
            slotReservation.setId(UUID.randomUUID().toString());
            slotReservation.setUserId(request.getUserId());
            slotReservation.setStatus(ReservationStatus.CREATED);
            slotReservation.setCreatedAt(Instant.now());
            return slotReservationRepository.save(slotReservation).doOnNext(createdReservationsMessagesProducer::sendMessage);
        } else {
            return Mono.error(new RuntimeException("Invalid slots"));
        }
    }

    /**
     * Check if the slots are valid
     *
     * @param reservation the reservation to check
     */
    private boolean checkSlotsValidity(ReserveDto reservation) {
        Instant now = Instant.now();
        Instant slotStart = reservation.getSlotStart();
        Instant slotEnd = reservation.getSlotEnd();

        try {
            DeliveryType type = DeliveryType.valueOf(reservation.getDeliveryType().toUpperCase());

            // check if the slot start and end are provided
            if (slotStart == null || slotEnd == null) {
                throw new RuntimeException("Slot start and end must be provided");
            }

            // check if the slot start is in the future
            if (slotStart.isAfter(now)) {
                throw new RuntimeException("Slot start must be in the future");
            }
            // check if the slot end is after the slot start
            if (slotEnd.isAfter(slotStart)) {
                throw new RuntimeException("Slot end must be after slot start");
            }

            // check the compatibility of the delivery type with the slots
            checkDeliveryTypeSlotsCompatibility(reservation, type, now, slotStart);

        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid delivery type: " + reservation.getDeliveryType(), e);
        }
        return true;
    }

    /**
     * Check if the delivery type is valid
     *
     * @param reservation the reservation to check
     * @param type        the delivery type
     */
    private void checkDeliveryTypeSlotsCompatibility(ReserveDto reservation, DeliveryType type, Instant now, Instant slotStart) {
        switch (type) {
            case DELIVERY -> {
            }
            case DELIVERY_TODAY -> {
                if (!slotStart.atZone(ZoneId.of("UTC")).toLocalDate().isEqual(now.atZone(ZoneId.of("UTC")).toLocalDate())) {
                    throw new RuntimeException("Delivery slot must be in the same day");
                }
            }
            case DELIVERY_ASAP -> {
                if (slotStart.isBefore(now.plusSeconds(3600))) {
                    throw new RuntimeException("Delivery slot must be at least 1 hour from now");
                }
            }
            case DRIVE -> {
                if (slotStart.isBefore(now.plusSeconds(1800))) {
                    throw new RuntimeException("Drive slot must be at least 30 minutes from now");
                }
            }
            default -> throw new IllegalArgumentException("Unknown delivery type: " + reservation.getDeliveryType());
        }
    }
}
