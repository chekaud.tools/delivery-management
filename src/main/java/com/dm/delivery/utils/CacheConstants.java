package com.dm.delivery.utils;

public class CacheConstants {
    public static final String CACHE_KEY = "#root.targetClass.simpleName.replaceAll('^(\\p{Lower}+).*', '$1')";
    public static final String CACHE_RESERVATION = "CACHE_RESERVATION";

}