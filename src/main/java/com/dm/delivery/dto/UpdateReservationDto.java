package com.dm.delivery.dto;

import lombok.Data;

@Data
public class UpdateReservationDto {

    private String status;

}
