package com.dm.delivery.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class SlotReservationDto {

    private String id;
    private Long userId;
    private Instant slotStart;
    private Instant slotEnd;
    private String address;
    private String note;
    private String deliveryType;

}
