package com.dm.delivery.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class ReserveDto {

    private Long userId;
    private Instant day;
    private Instant slotStart;
    private Instant slotEnd;
    private String address;
    private String phone;
    private String note;
    private String deliveryType;

}
