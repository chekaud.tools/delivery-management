package com.dm.delivery.dto.mapper;

import com.dm.delivery.domain.SlotReservation;
import com.dm.delivery.dto.ReserveDto;
import com.dm.delivery.dto.SlotReservationDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SlotReservationMapper {
    SlotReservationDto toDTO(SlotReservation slotReservation);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "status", ignore = true)
    SlotReservation fromMakeReservationRequest(ReserveDto request);
}
