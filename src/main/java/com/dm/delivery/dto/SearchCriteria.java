package com.dm.delivery.dto;

import lombok.Data;

@Data
public class SearchCriteria {
    private String status;
}
