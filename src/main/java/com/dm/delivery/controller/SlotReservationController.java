package com.dm.delivery.controller;

import com.dm.delivery.domain.enums.ReservationStatus;
import com.dm.delivery.dto.ReservationStatusDto;
import com.dm.delivery.dto.ReserveDto;
import com.dm.delivery.dto.SearchCriteria;
import com.dm.delivery.dto.SlotReservationDto;
import com.dm.delivery.dto.UpdateReservationDto;
import com.dm.delivery.dto.mapper.SlotReservationMapper;
import com.dm.delivery.service.SlotReservationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

import static com.dm.delivery.utils.CacheConstants.CACHE_KEY;
import static com.dm.delivery.utils.CacheConstants.CACHE_RESERVATION;

@RestController
@RequestMapping("/api/v1/reservations")
@AllArgsConstructor
public class SlotReservationController {

    private final SlotReservationService slotReservationService;
    private final SlotReservationMapper slotReservationMapper;

    @Operation(summary = "Get all reservations by user id and status")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reservations found"),
            @ApiResponse(responseCode = "500", description = "Something went wrong")})
    @PostMapping("/user/{id}/status")
    public Flux<SlotReservationDto> findAllReservationsByUserIdAndStatus(@PathVariable Long id, @RequestBody SearchCriteria criteria) {
        return slotReservationService.findAllReservationsByUserIdAndStatus(id, ReservationStatus.valueOf(criteria.getStatus())).map(slotReservationMapper::toDTO);
    }

    @Operation(summary = "Get a reservation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reservation found"),
            @ApiResponse(responseCode = "404", description = "Reservation not found")})
    @GetMapping("/{id}")
    public Mono<SlotReservationDto> getReservation(@PathVariable String id) {
        return slotReservationService.getReservation(id).map(slotReservationMapper::toDTO);
    }

    @Operation(summary = "Get all reservations by user id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reservations found"),
            @ApiResponse(responseCode = "500", description = "Something went wrong")})
    @GetMapping("/user/{id}")
    @Cacheable(cacheNames = CACHE_RESERVATION, key = "{" + CACHE_KEY + ", #id}")
    public Flux<SlotReservationDto> getReservationsByUserId(@PathVariable Long id) {
        return slotReservationService.findAllReservationsByUserId(id).map(slotReservationMapper::toDTO);
    }

    @Operation(summary = "Get possible delivery types for a reservation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Delivery types found"),
            @ApiResponse(responseCode = "500", description = "Something went wrong")})
    @GetMapping("/delivery-types")
    public Mono<List<String>> getDeliveryTypes() {
        return slotReservationService.getDeliveryTypes();
    }


    @Operation(summary = "Reserve a slot for delivery or drive through")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reservation created"),
            @ApiResponse(responseCode = "400", description = "Invalid input")})
    @PostMapping("/reserve")
    public Mono<SlotReservationDto> reserveSlot(@RequestBody ReserveDto request) {
        return slotReservationService.reserveSlot(request).map(slotReservationMapper::toDTO);
    }

    @Operation(summary = "Cancel a reservation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reservation canceled"),
            @ApiResponse(responseCode = "404", description = "Reservation not found")})
    @PostMapping("/cancel/{id}")
    public Mono<SlotReservationDto> cancelReservation(@PathVariable String id) {
        return slotReservationService.cancelReservation(id).map(slotReservationMapper::toDTO);
    }

    @Operation(summary = "Update a reservation status")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Reservation updated"),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "404", description = "Reservation not found")})
    @PostMapping("/{id}/update/status")
    public Mono<SlotReservationDto> updateReservationStatus(@PathVariable String id, @RequestBody UpdateReservationDto request) {
        return slotReservationService.updateReservationStatus(id, request).map(slotReservationMapper::toDTO);
    }

    @Operation(summary = "Get reservation status")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The reservation status"),
            @ApiResponse(responseCode = "500", description = "Something went wrong")})
    @GetMapping("/{id}/status")
    public Mono<ReservationStatusDto> getStatus(@PathVariable UUID id) {
        return slotReservationService.getStatus(id).map(ReservationStatusDto::new);
    }

}
