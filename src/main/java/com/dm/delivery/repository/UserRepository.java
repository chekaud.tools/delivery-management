package com.dm.delivery.repository;

import com.dm.delivery.domain.User;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface UserRepository extends ReactiveCrudRepository<User, Long> {

    @Query("SELECT * FROM users WHERE id = :id AND status = 'ENABLED'")
    Flux<User> findByIdAndStatus(Long id);

}
