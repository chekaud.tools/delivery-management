package com.dm.delivery.repository;

import com.dm.delivery.domain.SlotReservation;
import com.dm.delivery.domain.enums.ReservationStatus;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static com.dm.delivery.utils.CacheConstants.CACHE_KEY;
import static com.dm.delivery.utils.CacheConstants.CACHE_RESERVATION;


@Repository
public interface SlotReservationRepository extends ReactiveCrudRepository<SlotReservation, String> {

    Flux<SlotReservation> findAllByUserId(Long userId);

    Flux<SlotReservation> findAllByUserIdAndStatus(Long userId, ReservationStatus status);

    @Override
    @CacheEvict(value = CACHE_RESERVATION, key = "{" + CACHE_KEY + ", #entity.userId}")
    <S extends SlotReservation> Mono<S> save(S entity);
}
