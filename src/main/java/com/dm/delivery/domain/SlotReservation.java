package com.dm.delivery.domain;

import com.dm.delivery.domain.enums.DeliveryType;
import com.dm.delivery.domain.enums.ReservationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.time.Instant;

@Data
@Table("reservations")
@NoArgsConstructor
@AllArgsConstructor
public class SlotReservation implements Persistable<String> {

    @Id
    private String id;
    private Long userId;
    private Instant slotStart;
    private Instant slotEnd;
    private DeliveryType deliveryType;
    private String address;
    private String note;
    private Instant createdAt;
    private Instant updatedAt;
    private ReservationStatus status;

    @Override
    public boolean isNew() {
        return true;
    }
}
