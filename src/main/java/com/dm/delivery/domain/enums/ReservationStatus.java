package com.dm.delivery.domain.enums;

import lombok.Getter;

@Getter
public enum ReservationStatus {
    CREATED("CREATED"),
    PENDING("PENDING"),
    APPROVED("APPROVED"),
    CANCELED("CANCELED");

    private final String status;

    ReservationStatus(String status) {
        this.status = status;
    }

}
