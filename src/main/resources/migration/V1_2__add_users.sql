INSERT INTO users (id, name, email, phone, address, created_at, updated_at, status)
VALUES
    (1  , 'admin', 'admin@gmail.com', '1234567890', 'admin address', '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'ENABLED'),
    (2  , 'user1', 'user1@gmail.com', '1234567890', 'user1 address', '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'ENABLED'),
    (3  , 'user 2', 'user2@gmail.com', '1234567890', 'user2 address', '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'ENABLED'),
    (4  , 'user 3', 'user3@gmail.com', '1234567890', 'user3 address', '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'ENABLED'),
    (5  , 'user 4', 'user4@gmail.com', '1234567890', 'user4 address', '2020-01-01 00:00:00', '2020-01-01 00:00:00', 'ENABLED');
