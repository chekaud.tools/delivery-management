CREATE TABLE IF NOT EXISTS users (
                                        id int8 PRIMARY KEY,
                                        name VARCHAR(255),
                                        email VARCHAR(255),
                                        phone VARCHAR(255),
                                        address VARCHAR(255),
                                        created_at TIMESTAMP,
                                        updated_at TIMESTAMP,
                                        status VARCHAR(64)
);

CREATE TABLE IF NOT EXISTS reservations (
                                            id VARCHAR(64) PRIMARY KEY,
                                            user_id int8,
                                            slot_start TIMESTAMP,
                                            slot_end TIMESTAMP,
                                            delivery_type VARCHAR(255),
                                            address VARCHAR(255),
                                            note VARCHAR(255),
                                            created_at TIMESTAMP,
                                            updated_at TIMESTAMP,
                                            status VARCHAR(64),
                                            FOREIGN KEY (user_id) REFERENCES users(id)
);
